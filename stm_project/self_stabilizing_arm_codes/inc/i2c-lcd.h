/*
 * i2c-lcd.h
 *
 *  Created on: Dec 26, 2020
 *      Author: Maciej Wyleciał
 */

#include "stm32f4xx_hal.h"

void lcd_init (void);   // LCD panel initialization

void lcd_send_cmd (char cmd);  // Sending command to the LCD panel

void lcd_send_data (char data);  // Sending data to the LCD panel

void lcd_send_string (char *str);  // Sending string to the LCD panel

void lcd_set_cur(int row, int col); // Cursor placement

void lcd_clear (void);  // Clearing LCD panel

void lcd_welcome(void);  // welcome sequence
