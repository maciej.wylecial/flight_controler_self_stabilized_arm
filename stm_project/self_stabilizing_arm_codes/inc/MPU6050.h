/*
 * MPU6050.h
 *
 *  Created on: Jan 3, 2021
 *      Author: Maciej Wyleciał
 */

#include "stm32f4xx_hal.h"

void MPU6050_INIT (void);

void MPU6050_Read_Accel (void);

void MPU6050_Read_Gyro (void);

#define MPU6050_ADDR 0xD0

// Addresses are from MPU6050 datasheet
#define WHO_AM_I_REG 0x75	// Checking if the sensor is working, we should get 0x68 as an output
#define PWR_MGMT_1_REG 0x6B		// This register is responsible for waking up the sensor (Power management)
#define SMPLRT_DIV_REG 0x19		// Data sampling rate
#define ACCEL_CONFIG_REG 0x1C	// Full scale range of Accelerometer
#define ACCEL_XOUT_H_REG 0x3B	// Storing the most recent accelerometer measurements
#define GYRO_CONFIG_REG 0x1B	// Full scale range of Gyroscope
#define GYRO_XOUT_H_REG 0X43	// Storing the most recent gyroscope measurements
#define TEMP_OUT_H_REG 0x41		// Storing the most recent temperature measurements

int counter;

// Raw acceleration values from module
int16_t Accel_X_RAW;
int16_t Accel_Y_RAW;
int16_t Accel_Z_RAW;

// Raw gyro placement values from module
int16_t Gyro_X_RAW;
int16_t Gyro_Y_RAW;
int16_t Gyro_Z_RAW;

// Acceleration values after conversion
float Ax;
float Ay;
float Az;

// Gyroscope values after conversion
float Gx;
float Gy;
float Gz;


I2C_HandleTypeDef hi2c1; // Defining use of I2C1
