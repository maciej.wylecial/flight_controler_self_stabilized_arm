/*
 * MPU6050.c
 *
 *  Created on: Jan 10, 2021
 *      Author: Maciej Wyleciał
 */

#include "MPU6050.h"


void MPU6050_INIT (void) {
	uint8_t check, Data;
	//checking ID of the device
	HAL_I2C_Mem_Read(&hi2c1, MPU6050_ADDR, WHO_AM_I_REG, 1, &check, 1, 1000);

	if (check == 104) { // if the device is present 104 = 0x68 WHO_AM_I return
		//power management (register 0x6B) To wake up the sensor we have to write 0 values to the register
		Data = 0;
		HAL_I2C_Mem_Write(&hi2c1, MPU6050_ADDR, PWR_MGMT_1_REG, 1, &Data, 1, 1000);

		//set data rate in SMPLRT_DIV_REG
		Data = 0x07;  // setting 1kHz with DLPF enables (Dynamic Low Pass filter)
		HAL_I2C_Mem_Write(&hi2c1, MPU6050_ADDR, SMPLRT_DIV_REG, 1, &Data, 1, 1000);

		//set accelerometer configuration in ACCCEL_CONFIG Register
		// Setting AFS_SEL value to 0 provides full scale range of +/- 2'g'
		Data = 0x00; // The lower range the higher  accuracy we get
		HAL_I2C_Mem_Write(&hi2c1, MPU6050_ADDR, ACCEL_CONFIG_REG, 1, &Data, 1, 1000);

		//set gyroscope configuration in GYRO_CONFIG Register
		// Setting FS_SEL value to 0 provides full scale range of +/- 250 deg/s
		HAL_I2C_Mem_Write(&hi2c1, MPU6050_ADDR, GYRO_CONFIG_REG, 1, &Data, 1, 1000);
	}
}

void MPU6050_Read_Accel (void) {
	uint8_t Rec_Data[6];

	// Reading 6 bytes of RAW data as we need 2 bytes for every axis

	HAL_I2C_Mem_Read(&hi2c1, MPU6050_ADDR, ACCEL_XOUT_H_REG, 1, Rec_Data, 6, 1000);
	// Combining 6 byets into 16 bit integer values by shifting higher 8 bits to the left
	// Then using OR logic with lower 8 bits
	Accel_X_RAW = (int16_t)(Rec_Data[0] << 8 | Rec_Data[1]);
	Accel_Y_RAW = (int16_t)(Rec_Data[2] << 8 | Rec_Data[3]);
	Accel_Z_RAW = (int16_t)(Rec_Data[4] << 8 | Rec_Data[5]);

	counter++; // Global counter used to average readings for more stable values and easier debug
	// To convert RAW values into 'g' units as the Datasheet says the goal is to divide measurements by 16384.0
	// (For FS_SEL = 0 according 2'g' range with high accuracy)
	// Each MPU module and every axis read has different drift, to compensate it we have to add offset

	Ax = Accel_X_RAW/16384.0 + 0.04;
	Ay = Accel_Y_RAW/16384.0;
	Az = Accel_Z_RAW/16384.0 - 1.08;

}

void MPU6050_Read_Gyro (void) {
	uint8_t Rec_Data[6];

	// Reading 6 bytes of RAW data as we need 2 bytes of every axis

	HAL_I2C_Mem_Read(&hi2c1, MPU6050_ADDR, GYRO_XOUT_H_REG, 1, Rec_Data, 6, 1000);
	// Combining 6 byets into 16 bit integer values by shifting higher 8 bits to the left
	// Then using OR logic with lower 8 bit
	Gyro_X_RAW = (int16_t)(Rec_Data[0] << 8 | Rec_Data[1]);
	Gyro_Y_RAW = (int16_t)(Rec_Data[2] << 8 | Rec_Data[3]);
	Gyro_Z_RAW = (int16_t)(Rec_Data[4] << 8 | Rec_Data[5]);

	// Converting RAW values into dps, according to FS_SEL=0 and datasheet we have to divide by 131.0
	// As with acceleration to convert values into dps it is needed to divide measurements by 131.0
	// Each MPU module and every axis read has different drift, to compensate it we have to add offset

	Gx = (Gyro_X_RAW/131.0) - 5;
	Gy = Gyro_Y_RAW/131.0;
	Gz = (Gyro_Z_RAW/131.0) - 1.65;

}
