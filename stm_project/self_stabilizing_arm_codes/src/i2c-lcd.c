
#include "i2c-lcd.h"
extern I2C_HandleTypeDef hi2c1;

#define SLAVE_ADDRESS_LCD 0x4E // LCD address from Datasheet of our lab's LCD panel

void lcd_send_cmd (char cmd) {
  char data_u, data_l;
	uint8_t data_t[4];
	data_u = (cmd&0xf0);	  // Using LCD in 4 bit mode so we have to split 8 bit information
	data_l = ((cmd<<4)&0xf0); // First we have to send the higher 'quad' and then the lower one
	data_t[0] = data_u|0x0C;  //en=1, rs=0
	data_t[1] = data_u|0x08;  //en=0, rs=0
	data_t[2] = data_l|0x0C;  //en=1, rs=0
	data_t[3] = data_l|0x08;  //en=0, rs=0
	HAL_I2C_Master_Transmit (&hi2c1, SLAVE_ADDRESS_LCD,(uint8_t *) data_t, 4, 100);
}

void lcd_send_data (char data) {
	char data_u, data_l;
	uint8_t data_t[4];
	data_u = (data&0xf0);      // Using LCD in 4 bit mode so we have to split 8 bit information
	data_l = ((data<<4)&0xf0); // First we have to send the higher 'quad' and then the lower one
	data_t[0] = data_u|0x0D;   //en=1, rs=1
	data_t[1] = data_u|0x09;   //en=0, rs=1
	data_t[2] = data_l|0x0D;   //en=1, rs=1
	data_t[3] = data_l|0x09;   //en=0, rs=1
	HAL_I2C_Master_Transmit (&hi2c1, SLAVE_ADDRESS_LCD,(uint8_t *) data_t, 4, 100);
}

void lcd_clear (void) {
	// The easisesy wayy of clearing the panel is to write blank sign
	lcd_send_cmd (0x00);
	for (int i=0; i<100; i++)
	{
		lcd_send_data (' ');
	}
}

void lcd_set_cur(int row, int col) {
	switch(row) { // Row can be only only 0 or 1 while the column stays between 0 and 15
	case 0:
		col |= 0x80;
		break;
	case 1:
		col |= 0xC0;
		break;
	}
	lcd_send_cmd (col);
}

void lcd_init (void)
{
	// Pattern of the initialization based on the Datasheet
	// 4 bit initialization
	HAL_Delay(50);  // wait for >40ms
	lcd_send_cmd (0x30);
	HAL_Delay(5);  // wait for >4.1ms
	lcd_send_cmd (0x30);
	HAL_Delay(1);  // wait for >100us
	lcd_send_cmd (0x30);
	HAL_Delay(10);
	lcd_send_cmd (0x20);  // 4bit mode
	HAL_Delay(10);

  // display initialization
	lcd_send_cmd (0x28); // Value sending function setup
	HAL_Delay(1);
	lcd_send_cmd (0x08); // Display on/off
	HAL_Delay(1);
	lcd_send_cmd (0x01); // Clearing the display
	HAL_Delay(1);
	HAL_Delay(1);
	lcd_send_cmd (0x06);  // Entry mode set
	HAL_Delay(1);
	lcd_send_cmd (0x0C);  // Controlling turning on/off
}

void lcd_welcome(void) { // This function only initializes the panel and shows 'title screen'
	lcd_init();

	lcd_send_string("Initialized");

	HAL_Delay(1000);

	lcd_clear();

	lcd_send_cmd(0x80|0x00);
	lcd_send_string("MPU6050");

	HAL_Delay(1000);
}

void lcd_send_string (char *str) { // This function allows to send the whole string to the LCD panel
	while (*str) lcd_send_data (*str++);
}


